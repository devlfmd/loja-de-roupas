'use strict'

const express = require('express');
const Route = express.Router();

const CategoryController = require('../app/controllers/CategoryController');
const ProductController = require('../app/controllers/ProductController');

/**
 * Rotas de categorias de produtos
 */
Route.get("/categorias/", async (req, res, next) => (await new CategoryController().getAll(req, res, next)));

/**
 * Rotas de produtos
 */
Route.get("/produtos/", async (req, res, next) => (await new ProductController().getAll(req, res, next)));
Route.get("/produtos/categoria/:id", async (req, res, next) => (await new ProductController().getByCategory(req, res, next)));

module.exports = Route;