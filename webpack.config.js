const path = require('path');

module.exports = {
    entry: ['@babel/polyfill', './public/index.js'],
    mode: 'development',
    output: {
        filename: 'bundle.js',
        publicPath: '/public',
        path: path.resolve(__dirname, 'public/build')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets" : ["env", "react"]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [{ loader: 'css-loader' }]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [{ loader: 'file-loader' }],
            }
        ]
    }
};