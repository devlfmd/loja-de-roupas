'use strict'

const categories = require('../../__mocks__/categories.json');

class CategoryController { 

    /**
     * Retorna todas as categorias de produto
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async getAll(req, res, next) {
        return res.send(categories).status(200); 
    };
};

module.exports = CategoryController;