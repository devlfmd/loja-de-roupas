'use strict'

const products = require('../../__mocks__/products.json');

class ProductController { 
    /**
     * Retorna todos os produtos de acordo com categoria
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async getByCategory(req, res, next) {
        const { id } = req.params;
        return res.send(products[id]).status(200); 
    };

    /**
     * Retorna todos os produtos
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async getAll(req, res, next) {
        let items = [];
        products.forEach((product) => {
            product.items.forEach((item) => {
                items.push(item.name)
            });
        });
        return res.send(items).status(200); 
    };
};

module.exports = ProductController;