import React from 'react';
import HomeLayout from './components/HomeLayout';
import { 
    Home,
    Products,
    Contact
} from './views/index';
  
  const routes = (loggedIn) => [
    { 
        path: '', 
        element: <HomeLayout />,
        children: [
            { 
                path: '', 
                element: <Home /> 
            },
            { 
                path: '/produtos',
                children: [
                    { 
                        path: '/categoria/:id', 
                        element: <Products /> 
                    }
                ]
            },
            { 
                path: '/contato', 
                element: <Contact /> 
            }
        ],
    }
];

export default routes;
