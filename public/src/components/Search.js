import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/core/Autocomplete';

const Search = ({ options, placeholder, label, CustomRender, onChange, value }) => (
  <div style={{ width: 300 }}>
    <Autocomplete
      options={options}
      autoComplete={true}
      value={value}
      noOptionsText="Não encontramos o que você pesquisou"
      renderInput={(params, id) => (
        CustomRender != undefined ? (<CustomRender />) : (
          <TextField 
            key={id}
            {...params}
            onChange={(e) => onChange(e.target.value)} 
            label={label} 
            placeholder={placeholder} 
            size="small" 
            margin="none" 
            variant="outlined" 
          />
        )
      )}
    />
  </div>
);

export default Search;