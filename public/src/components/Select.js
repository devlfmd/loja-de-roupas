import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const SelectInput = ({ size, variant, title, value, onChange, label, placeholder, options }) => (
    <FormControl size={size} variant={variant}>
        <InputLabel>{title}</InputLabel>
        <Select
            value={value}
            onChange={(e) => onChange(e.target.value)}
            label={label}
            placeholder={placeholder}
        >
            {options.map((option) => (
                <MenuItem value={option.value}>{option.title}</MenuItem>
            ))}
        </Select>
    </FormControl>
);

export default SelectInput;