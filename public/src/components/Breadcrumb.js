import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Breadcrumbs, Button } from '@material-ui/core';

const Breadcrumb = ({ links }) => {
    const navigate = useNavigate();
    return (
        <Breadcrumbs separator="›" aria-label="breadcrumb">
            {links.map(({ title, link }) => (<Button color="inherit" onClick={() => navigate(link)}> { title } </Button>))}
        </Breadcrumbs>       
    );
};

export default Breadcrumb;