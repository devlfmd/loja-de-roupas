import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
    AppBar,
    Toolbar,
    Button,
    Box,
    Typography,
    Hidden
} from '@material-ui/core';
import { getAll } from '../store/category/actions';
import { getAll as getAllProducts } from '../store/product/actions';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';

import Logo from '../assets/webjump.jpeg';

import Search from './Search';

const useStyles = makeStyles({
    topBar: {
        background: '#231f20',
        color: '#FFF',
        padding: '10px'
    },
    navBar: {
        background: '#FFF'
    }
});

const NavBar = () => {
    const classes = useStyles();
    const navigate = useNavigate();

    const [categories, setCategories] = React.useState([]);
    const [products, setProducts] = React.useState([]);
    const [search, setSearch] = React.useState('');

    /**
     * Consulta todas categorias
     */
    const fetchCategories = async() => {
        const req = await getAll();
        if(req.data)
            setCategories(req.data);
        else
            setCategories([]);
    };

    /**
     * Consulta todos produtos
     */
    const fetchProducts = async() => {
        const req = await getAllProducts();
        if(req.data)
            setProducts(req.data);
        else
            setProducts([]);
    }

    React.useEffect(() => { 
        fetchCategories(); 
        fetchProducts();
    }, []);

    return (
        <React.Fragment>
            <Box className={classes.topBar} display="flex" flexDirection="row" justifyContent="flex-end" sx={{ height: 40 }}>
                <Link to="/">
                    <Typography color="#FFF">Acesse a sua conta</Typography>
                </Link>
                <Typography color="#FFF">&nbsp; ou &nbsp;</Typography>
                <Link to="/">
                    <Typography color="#FFF">Cadastre-se</Typography>
                </Link>
            </Box>
            <Box 
                className={classes.navBar} 
                display="flex"
                flexDirection="row" 
                alignItems="center"
                justifyContent="space-between"
                padding="10px"
            >
                <Hidden mdUp>
                    <Box>
                        <Button>
                            <MenuIcon style={{ color: '#000' }} />
                        </Button>
                    </Box>
                </Hidden>
                <Box>
                    <img width="128px" height="64px" src={Logo} />
                </Box>
                <Hidden mdUp>
                    <Button>
                        <SearchIcon color="primary" />
                    </Button>
                </Hidden>
                <Hidden mdDown>
                    <Box 
                        display="flex" 
                        flexDirection="row" 
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <Search 
                            placeholder="Digite aqui para pesquisar um produto"
                            options={products}
                            onChange={(s) => setSearch(s)}
                        />
                        <Button color="primary" size="medium" variant="outlined">Buscar</Button>
                    </Box>
                </Hidden>
            </Box>
            <Hidden mdDown>
                <AppBar position="static" color="primary">
                    <Toolbar>
                        <Button 
                            color="inherit"
                            onClick={() => navigate(`/`)}
                        >
                            <Typography fontWeight="bold">Página inicial</Typography>
                        </Button>
                            {categories.map(({ id, name }) => (
                                <Button 
                                    color="inherit"
                                    key={id}
                                    className={classes.bold}
                                    onClick={() => navigate(`/produtos/categoria/${id}`)}
                                >
                                    <Typography fontWeight="bold">
                                        {name}
                                    </Typography>
                                </Button>
                            ))}  
                        <Button 
                            color="inherit"
                            onClick={() => navigate(`/`)}
                        >
                            <Typography fontWeight="bold">Contato</Typography>
                        </Button>
                    </Toolbar>
                </AppBar>
            </Hidden>
        </React.Fragment>
    );
};

export default NavBar;