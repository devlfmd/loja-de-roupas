import React from 'react';
import Navbar from './NavBar';
import Footer from './Footer';
import { Outlet } from 'react-router-dom';

const HomeLayout = () => (
    <React.Fragment>
        <Navbar />
            <Outlet />
        <Footer />
    </React.Fragment>
);

export default HomeLayout;