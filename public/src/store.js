import { configureStore } from '@reduxjs/toolkit';
import authSlice from './store/auth/reducer';
import productSlice from './store/product/reducer';
import categorySlice from './store/category/reducer';

export const store = configureStore({
    reducer: {
        auth: authSlice,
        product: productSlice,
        category: categorySlice
    },
});