import axiosLib from 'axios';

/**
 * Hook para uso do axios
 */
export function useAxios() { 
    const axios = axiosLib.create({
        baseURL: '/api/V1',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
        }
    });

    axios.interceptors.request.use(
        function (config) {
            return Promise.resolve(config);
        }, 
        function (error) {
            return Promise.reject(error);
        }
    );
    
    axios.interceptors.response.use(
        function (response) {
            return response;
        }, 
        function (error) {
            return error;
        }
    );
  
    return { axios };
};