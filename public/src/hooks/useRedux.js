import { useDispatch, useSelector } from 'react-redux';

/**
 * Hook para uso do redux
 */
export function useRedux() {
    const useAppDispatch = () => useDispatch();
    const useAppSelector = useSelector;

    return { useAppDispatch, useAppSelector };
};