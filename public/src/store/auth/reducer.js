import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    token: '',
    username: '',
    loggedIn: false
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        signIn: (state, action) => {
            const { username, token } = action.payload;

            state.username = username;
            state.token = token;
            state.loggedIn = true;

            localStorage.setItem('token', token);
        },
        signOut: (state) => {
            localStorage.removeItem('token');
            state.loggedIn = false;
        }
    },
});

export const { signIn } = authSlice.actions;
export default authSlice.reducer;