import { useAxios } from '../../hooks/useAxios';

const { axios } = useAxios();
const endpoint = 'categorias';

/**
 * Retorna categorias de produtos
 */
const getAll = async () => (await axios.get(`/${endpoint}`));

export { getAll };