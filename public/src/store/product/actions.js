import { useAxios } from '../../hooks/useAxios';

const { axios } = useAxios();
const endpoint = 'produtos';

/**
 * Retorna todos produtos por categoria
 */
const getByCategory = async (id) => (await axios.get(`/${endpoint}/categoria/${id}`));

/**
 * Retorna todos produtos
 */
const getAll = async () => (await axios.get(`/${endpoint}`));

export { getByCategory, getAll };
