import React from 'react';
import { useParams } from 'react-router-dom';
import { getByCategory } from '../store/product/actions';
import { getAll } from '../store/category/actions';
import { Helmet } from 'react-helmet-async';
import { makeStyles } from '@material-ui/styles';
import { Box, Button, Typography, Container, Divider, Hidden } from '@material-ui/core';
import  Breadcrumb  from '../components/Breadcrumb.js';
import AppsIcon from '@material-ui/icons/Apps'; 
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Select from '../components/Select';

const useStyles = makeStyles({
    flexContainer: {
        display: 'flex', 
        flexDirection: 'row', 
        justifyContent: 'space-around',
        marginTop: '20px'
    },
    filterContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between', 
        flexWrap: 'wrap',
        height: 'auto',
        padding: '16px'
    },
    productContainerGrid: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start'
    },
    productContainerList: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        justifyContent: 'flex-start'
    },
    filterCard: { 
        border: '1px solid gray', 
        height: 'auto !important',
        padding: '10px',
        width: '235px',
        display: 'flex',
        flexDirection: 'column'
    },
    productCardGrid: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: '8px',
        marginLeft: '5px',
        marginRight: '5px',
        marginBottom: '20px',
    },
    productCardList: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        marginLeft: '5px',
        marginRight: '5px',
        borderRadius: '8px',
        marginBottom: '20px',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    productImg: { 
        border: '1px solid gray', 
        maxHeight: '250px', 
        height: '250px', 
        width: '100%' 
    },
    btnComprar: {
        background: '#80bdb8 !important',
        width: '100%'
    }
});

export const Products = () => {
    const classes = useStyles();
    const { id } = useParams();
    const [products, setProducts] = React.useState([]);
    const [categories, setCategories] = React.useState([]);
    const [filters, setFilters] = React.useState([]);
    const [moneyFilter, setMoneyFilter] = React.useState('asc');
    const [listMode, setListMode] = React.useState('grid');

    /**
     * Retorna todos os produtos
     */
    const fetchProducts = async (id) => {
        const req = await getByCategory(id);
        if(req.data.items && req.data.filters) {
            if(moneyFilter === 'asc')
                setProducts(req.data.items.sort((a,b) => a.price > b.price ));
            else    
                setProducts(req.data.items.sort((a,b) => a.price < b.price ));
            setFilters(req.data.filters);
        } else
            setProducts([]);
    };

    /**
     * Retorna todas categorias
     */
    const fetchCategories = async () => {
        const req = await getAll();
        if(req.data)
            setCategories(req.data);
        else
            setCategories([]);
    };

    /**
     * Filtra por gênero
     * @param {*} value 
     */
    const filterByGender = (value) => {
        setProducts(
            products.filter((product) => product.filter[0].gender === value[0])
        );
    };

    /**
     * Filtra por cor
     * @param {*} value 
     */
    const filterByColor = (value) => {
        setProducts(
            products.filter((product) => product.filter[0].color === value[0])
        );
    };

    /**
     * Método para limpar filtro
     */
    const clearFilter = async () => { await fetchProducts(id) };

    React.useEffect(() => { 
        fetchProducts(id);
        fetchCategories();
    }, [id, moneyFilter]);

    return (
        <React.Fragment>
            <Helmet>
                <title> {`Catálogo de produtos | Modernizar`} </title>
            </Helmet>
            <Container sx={{ marginTop: '40px', marginBottom: '200px' }}>
                <Breadcrumb links={[
                    { title: 'Página inicial', link: '/' },
                    categories.length > 0 ? (
                        { title: categories[id].name, link: `/produtos/categoria/${categories[id].id}` }
                    ) : ({})
                ]} />
                <Hidden mdDown>
                    <Box flexWrap={listMode === 'grid' ? ('nowrap') : ('wrap')} className={classes.flexContainer}>
                        <Box>
                            <Box className={classes.filterCard}>
                                <Typography variant="h3" color="primary" fontWeight="bold">FILTRE POR</Typography>
                                {filters.map((item) => (
                                    <React.Fragment>
                                        <Typography fontWeight="bold" variant="h5" color="secondary">
                                            {item[Object.keys(item)[0]].toUpperCase()}
                                        </Typography>
                                        <div className={classes.filterContainer}>
                                            {products.map((product) => product.filter)
                                                .map((filter, id) => {
                                                    const filterValue = filter.map((filterItem) => (filterItem[Object.keys(item)[0]]));
                                                    return (
                                                        (item[Object.keys(item)[0]] === 'Cores') ? (
                                                            <Button
                                                                key={id}
                                                                onClick={() => filterByColor(filterValue)}
                                                                style={{ 
                                                                    backgroundColor: `${filterValue}`, 
                                                                    height: '32px', 
                                                                    borderRadius: 0,
                                                                    margin: '3px'
                                                                }} 
                                                            />
                                                        ) : (item[Object.keys(item)[0]] === 'Gênero') ? (
                                                            <li 
                                                                key={id} 
                                                                onClick={() => filterByGender(filterValue)}
                                                                style={{ cursor: 'pointer' }}
                                                            >
                                                                {filterValue}
                                                            </li>
                                                        ) : (null)
                                                    );
                                                }
                                            )}
                                            <Box style={{ width: 100 + '%', marginTop: '20px' }}>
                                                <Divider />
                                                <Button 
                                                    style={{ width: 100 + '%' }}
                                                    onClick={async () => await clearFilter()}
                                                >
                                                    Limpar filtros
                                                </Button>
                                            </Box>
                                        </div>
                                    </React.Fragment>
                                ))}
                            </Box>
                        </Box>
                        <Box>
                            <Box marginLeft="5px" marginBottom="15px" >
                                <Typography marginBottom="10px" variant="h2" fontWeight="bold" color="primary">{categories.length > 0 ? (categories[id].name) : ('')}</Typography>
                                <Divider />
                                <Box padding="5px" display="flex" flexDirection="row" justifyContent="space-between">
                                    <Box>
                                        <Button onClick={() => setListMode('grid')}>
                                            <AppsIcon color={listMode === 'grid' ? ('secondary') : ('default')} />
                                        </Button>
                                        <Button onClick={() => setListMode('list')}>
                                            <FormatListBulletedIcon color={listMode === 'list' ? ('secondary') : ('default')} />
                                        </Button>
                                    </Box>
                                    <Box>
                                        <Select 
                                            size="small"
                                            variant="outlined"
                                            title="Ordernar por: "
                                            value={moneyFilter}
                                            onChange={(value) => setMoneyFilter(value)}
                                            label="Ordernar por: "
                                            placeholder="Ordernar por: "
                                            options={[
                                                { value: 'asc', title: 'Menor preço' },
                                                { value: 'desc', title: 'Maior preço' }
                                            ]}
                                        />
                                    </Box>
                                </Box>
                                <Divider />
                            </Box>
                            {listMode !== undefined ? (
                                <Box className={listMode === 'grid' ? classes.productContainerGrid : classes.productContainerList}>
                                    {products.map(({
                                        id,
                                        image,
                                        name,
                                        price,
                                        specialPrice
                                    }) => (
                                        <Box key={id} className={listMode === 'grid' ? classes.productCardGrid : classes.productCardList}>
                                            <img className={classes.productImg} src={`http://localhost:8888/${image}`} />
                                            <Typography marginBottom="10px" fontWeight="bold" color="gray" variant="h5">{name.toUpperCase()}</Typography>
                                            {specialPrice ? (
                                                <React.Fragment>
                                                    <Typography color="primary" fontWeight="bold" variant="h5">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(price)))}</Typography>
                                                    <Typography style={{ color: 'gray', textDecoration: 'line-through' }} variant="h4">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(specialPrice)))}</Typography>
                                                </React.Fragment>
                                            ) : (
                                                <Typography color="primary" fontWeight="bold" variant="h5">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(price)))}</Typography>
                                            )}
                                            <Button className={classes.btnComprar} style={{ marginTop: '20px'}}>
                                                <Typography color="#FFF" fontWeight="bold">{`Comprar`.toUpperCase()}</Typography>
                                            </Button>
                                        </Box>
                                    ))}
                                </Box>    
                            ) : (null)}
                        </Box>
                    </Box>
                </Hidden>
                <Hidden mdUp>
                    <Box flexWrap={listMode === 'grid' ? ('wrap') : ('wrap')} className={classes.flexContainer}>
                        <Box>
                            <Box className={classes.filterCard}>
                                <Typography variant="h3" color="primary" fontWeight="bold">FILTRE POR</Typography>
                                {filters.map((item) => (
                                    <React.Fragment>
                                        <Typography fontWeight="bold" variant="h5" color="secondary">
                                            {item[Object.keys(item)[0]].toUpperCase()}
                                        </Typography>
                                        <div className={classes.filterContainer}>
                                            {products.map((product) => product.filter)
                                                .map((filter, id) => {
                                                    const filterValue = filter.map((filterItem) => (filterItem[Object.keys(item)[0]]));
                                                    return (
                                                        (item[Object.keys(item)[0]] === 'Cores') ? (
                                                            <Button
                                                                key={id}
                                                                onClick={() => filterByColor(filterValue)}
                                                                style={{ 
                                                                    backgroundColor: `${filterValue}`, 
                                                                    height: '32px', 
                                                                    borderRadius: 0,
                                                                    margin: '3px'
                                                                }} 
                                                            />
                                                        ) : (item[Object.keys(item)[0]] === 'Gênero') ? (
                                                            <li 
                                                                key={id} 
                                                                onClick={() => filterByGender(filterValue)}
                                                                style={{ cursor: 'pointer' }}
                                                            >
                                                                {filterValue}
                                                            </li>
                                                        ) : (null)
                                                    );
                                                }
                                            )}
                                            <Box style={{ width: 100 + '%', marginTop: '20px' }}>
                                                <Divider />
                                                <Button 
                                                    style={{ width: 100 + '%' }}
                                                    onClick={async () => await clearFilter()}
                                                >
                                                    Limpar filtros
                                                </Button>
                                            </Box>
                                        </div>
                                    </React.Fragment>
                                ))}
                            </Box>
                        </Box>
                        <Box>
                            <Box marginLeft="5px" marginBottom="15px" >
                                <Typography marginBottom="10px" variant="h2" fontWeight="bold" color="primary">{categories.length > 0 ? (categories[id].name) : ('')}</Typography>
                                <Divider />
                                <Box padding="5px" display="flex" flexDirection="row" justifyContent="space-between">
                                    <Box>
                                        <Button onClick={() => setListMode('grid')}>
                                            <AppsIcon color={listMode === 'grid' ? ('secondary') : ('default')} />
                                        </Button>
                                        <Button onClick={() => setListMode('list')}>
                                            <FormatListBulletedIcon color={listMode === 'list' ? ('secondary') : ('default')} />
                                        </Button>
                                    </Box>
                                    <Box>
                                        <Select 
                                            size="small"
                                            variant="outlined"
                                            title="Ordernar por: "
                                            value={moneyFilter}
                                            onChange={(value) => setMoneyFilter(value)}
                                            label="Ordernar por: "
                                            placeholder="Ordernar por: "
                                            options={[
                                                { value: 'asc', title: 'Menor preço' },
                                                { value: 'desc', title: 'Maior preço' }
                                            ]}
                                        />
                                    </Box>
                                </Box>
                                <Divider />
                            </Box>
                            {listMode !== undefined ? (
                                <Box className={listMode === 'grid' ? classes.productContainerGrid : classes.productContainerList}>
                                    {products.map(({
                                        id,
                                        image,
                                        name,
                                        price,
                                        specialPrice
                                    }) => (
                                        <Box key={id} style={{ width: '100%' }} className={listMode === 'grid' ? classes.productCardGrid : classes.productCardList}>
                                            <img className={classes.productImg} src={`http://localhost:8888/${image}`} />
                                            <Typography marginBottom="10px" fontWeight="bold" color="gray" variant="h5">{name.toUpperCase()}</Typography>
                                            {specialPrice ? (
                                                <React.Fragment>
                                                    <Typography color="primary" fontWeight="bold" variant="h5">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(price)))}</Typography>
                                                    <Typography style={{ color: 'gray', textDecoration: 'line-through' }} variant="h4">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(specialPrice)))}</Typography>
                                                </React.Fragment>
                                            ) : (
                                                <Typography color="primary" fontWeight="bold" variant="h5">{(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Number(price)))}</Typography>
                                            )}
                                            <Button className={classes.btnComprar} style={{ marginTop: '20px'}}>
                                                <Typography color="#FFF" fontWeight="bold">{`Comprar`.toUpperCase()}</Typography>
                                            </Button>
                                        </Box>
                                    ))}
                                </Box>    
                            ) : (null)}
                        </Box>
                    </Box>
                </Hidden>
            </Container>
        </React.Fragment>
    );
};