import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async'

import { Box, Container, Typography, Hidden } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { getAll } from '../store/category/actions';

const useStyles = makeStyles({
    menu: {
        background: '#e2dedb',
        width: '250px',
        padding: '40px',
        margin: '20px'
    },
    box1: {
        height: '127px',
        background: '#acacac',
        margin: '20px'
    },
    box2: {
        margin: '20px'
    },
    container: {
        marginBottom: '25vh'
    },
    cursorPointer: {
        cursor: 'pointer'
    }
});

export const Home = () => {
    const classes = useStyles();
    const navigate = useNavigate();
    const [categories, setCategories] = React.useState([]);

    /**
     * Consulta todas categorias
     */
    const fetchCategories = async() => {
        const req = await getAll();
        if(req.data)
            setCategories(req.data);
        else
            setCategories([]);
    };

    React.useEffect(() => { fetchCategories() }, []);
    
    return (
        <React.Fragment>
            <Helmet>
                <title> {`Página inicial | Modernizar`} </title>
            </Helmet>
            <Container className={classes.container}>
                <Box 
                    display="flex" 
                    flexDirection="row" 
                    justifyContent="space-around" 
                    marginTop="10vh"
                    marginBottom="10vh"
                >
                    <Hidden mdDown>
                        <Box className={classes.menu}>
                            <ul>
                                <li 
                                    className={classes.cursorPointer}
                                    onClick={() => navigate(`/produtos/categoria/${id}`)
                                }>
                                    Página inicial
                                </li>
                                    {categories.map(({ id, name }) => (
                                        <li 
                                            key={id}
                                            className={classes.cursorPointer}
                                            onClick={() => navigate(`/produtos/categoria/${id}`)}
                                        >
                                            {name}
                                        </li>
                                    ))}
                                <li
                                    className={classes.cursorPointer}
                                    onClick={() => navigate(`/contato`)}
                                >
                                    Contato
                                </li>
                            </ul>
                        </Box>
                    </Hidden>

                    <Box>
                        <Box className={classes.box1}></Box>
                        <Box className={classes.box2}>
                            <Typography variant="h3">Seja bem-vindo !</Typography>
                            <Typography variant="span">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </Typography>
                        </Box>
                    </Box>
                </Box>
            </Container>
        </React.Fragment>
    );
};