import React from 'react';
import { Helmet } from 'react-helmet-async';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({});

export const Contact = () => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Helmet>
                <title> {`Contato | Modernizar`} </title>
            </Helmet>
        </React.Fragment>
    );
};