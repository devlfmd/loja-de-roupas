'use strict'

const express = require('express');
const cors = require('cors');
const compression = require('compression');
const serveStatic = require('serve-static');
const webpack = require('webpack');
const bodyParser = require('body-parser').json(require('./config/bodyParser.json'));

const config = require('./webpack.config');
const routes = require('./start/routes');
const app = express();
const compiler = webpack(config);
const corsConfig = require('./config/cors.json');

app.use(cors(corsConfig))
  .use(bodyParser)
  .use(compression())
  .use('/', serveStatic('public', {'index': ['index.html']}))
  .use('/media', express.static('./public/media'))
  .use('/api/V1', routes)
  .use(require('webpack-dev-middleware')(compiler, { publicPath: config.output.publicPath }))
  .listen(8888, () => { console.log('Acesse: http://localhost:8888') });